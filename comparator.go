package comparator

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

const (
	equal            = " == "           // left equal right, works with integer values only
	not_equal        = " != "           // left not equal right, works with integer values only
	greater          = " > "            // left greater right, works with integer values only
	less             = " < "            // left less right, works with integer values only
	greater_or_equal = " >= "           // left greater or equal right works with integer values only
	less_or_equal    = " <= "           // left value less or equal right value, works with integer values only
	contains         = " contains "     // the line from the left side contains the value from the right side
	has_prefix       = " has_prefix "   // the line from the left side begins with the content of the right side
	has_suffix       = " has_suffix "   // the line from the left side ends with the content of the right side
	in               = " in "           // left value is in a list right side
	not_in           = " not_in "       // left value is not in a list right side
	match_regexp     = " match_regexp " // left value matches a regular expression in right side of pattern
)

// operators supported compare operators
var operators = []string{
	equal,
	not_equal,
	greater,
	less,
	greater_or_equal,
	less_or_equal,
	contains,
	has_prefix,
	has_suffix,
	in,
	not_in,
	match_regexp,
}

// Comparator comparing by pattern-expression
type Comparator struct {
}

// construct
func NewComparator() *Comparator {
	return &Comparator{}
}

// Condition native structure to comparison
type Condition struct {
	operator string
	left     string
	right    string
}

// parse parsing pattern-condition string
func (c *Comparator) Parse(pattern string) (*Condition, error) {
	unlist := func(x []string) (string, string) {
		return strings.TrimSpace(x[0]), strings.TrimSpace(x[1])
	}

	for _, o := range operators {
		x := strings.Split(pattern, o)
		if len(x) == 2 {
			left, right := unlist(x)

			return &Condition{
				operator: o,
				left:     strings.ToLower(left),
				right:    strings.ToLower(right),
			}, nil
		}
	}

	return nil, fmt.Errorf("usupported operator: %s", pattern)
}

// Compare native compare realization
func (c *Comparator) Compare(cond *Condition) bool {
	switch cond.operator {
	case equal:
		return strings.ToLower(cond.left) == strings.ToLower(cond.right)
	case not_equal:
		return strings.ToLower(cond.left) != strings.ToLower(cond.right)
	case greater:
		l, _ := strconv.ParseInt(cond.left, 0, 32)
		r, _ := strconv.ParseInt(cond.right, 0, 32)
		return l > r
	case less:
		l, _ := strconv.ParseInt(cond.left, 0, 32)
		r, _ := strconv.ParseInt(cond.right, 0, 32)
		return l < r
	case greater_or_equal:
		l, _ := strconv.ParseInt(cond.left, 0, 32)
		r, _ := strconv.ParseInt(cond.right, 0, 32)
		return l >= r
	case less_or_equal:
		l, _ := strconv.ParseInt(cond.left, 0, 32)
		r, _ := strconv.ParseInt(cond.right, 0, 32)
		return l <= r
	case contains:
		return strings.Contains(cond.left, cond.right)
	case has_prefix:
		return strings.HasPrefix(cond.left, cond.right)
	case has_suffix:
		return strings.HasSuffix(cond.left, cond.right)
	case in:
		slice := strings.Split(cond.right, ",")
		for _, s := range slice {
			if strings.TrimSpace(s) == cond.left {
				return true
			}
		}
		return false
	case not_in:
		slice := strings.Split(cond.right, ",")
		for _, s := range slice {
			if strings.TrimSpace(s) == cond.left {
				return false
			}
		}
		return true
	case match_regexp:
		regexp, _ := regexp.Compile(cond.right)
		return regexp.MatchString(cond.left)
	}

	return false
}
